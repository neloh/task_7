﻿#include <iostream>
#include <string>
#include <fstream>
#include <malloc.h>

/*Найти максимальный среди элементов тех столбцов, которые упорядочены либо по
возрастанию, либо по убыванию. Если упорядоченные столбцы в матрице отсутствуют,
то вывести 0.*/

/*Лобанов Даниил Геннадьевич*/

int bounded_input(int, int);
bool check_increase(int, int);
bool check_decrease(int, int);
bool check_monotany(int**, int, int, bool (*comp)(int, int));
int find_max_in_monotany(int**, int, int);
int complete_task(int**, int, int);
void destroy(int**, int);
void read_row_from_file(std::istream&, int*, int);
int** read_from_file(int&, int&);
void read_row_from_console(int*, int);
int** read_from_console(int&, int&);
int** read_data(int, int&, int&);
void write_result_in_file(std::string);
void write_result_in_console(std::string);
void write_result(int option, std::string);


void menu();

int main()
{
    setlocale(LC_ALL, "Russian");
    menu();
}

int bounded_input(int low, int high)
{
    int value;
    for (;;)
    {
        std::cin >> value;
        if ((value >= low) && (value <= high))
        {
            return value;
        }

        std::cout << "Введено некорректное значение" << std::endl;
    }
}


bool check_increase(int a, int b)
{
    return a >= b;
}

bool check_decrease(int a, int b)
{
    return a <= b;
}


bool check_monotany(int** mt, int j, int n, bool (*comp)(int, int))
{

    // Ну вообще-то, есть задача проверки всех элементов на удовлетворение условию и есть задача на проверку 
    // наличия хотя бы одного элемента, удовлетворяющего условию.
    // Обе эти задачи сводятся друг к другу, и эффективнее решать вторую. В твоем случае решается первая, да еще корявенько. 
    // Решать надо так: 
    //     для каждого элемента : если элемент не удовлетворяет условию -> выход из функции с результатом false
    //     вернуть true

    // Исправлено:

    for (int i = 1, k = 2; (i < n - 1); i++, k++)
    {
        if (!comp(mt[i][j], mt[k][j]))
        {
            return false;
        }
    }

    return true;
}

// Кстати, упорядоченнность не предполагает, что все элементы разные
// Ты либо скобки везде ставь
// либо не ставь скобки нигде

// Исправлено:
int find_max_in_monotany(int** mt, int j, int n)
{

    if (check_increase(mt[0][j], mt[1][j]))
    {
        if (check_monotany(mt, j, n, check_increase))
        {
            return mt[0][j];
        }
    }
    else
    {
        if (check_monotany(mt, j, n, check_decrease))
        {
            return mt[n - 1][j];
        }
    }

    return 0;

}

int complete_task(int** mt, int n, int m)
{
    int j = 0;
    int max_in_column = find_max_in_monotany(mt, j, n);
    int res = max_in_column;

    for (j = 1; j < m; j++)
    {
        max_in_column = find_max_in_monotany(mt, j, n);

        if (res < max_in_column)
        {
            res = max_in_column;
        }
    }
    return res;
}

void destroy(int** mt, int n)
{
    for (int i = 1; i < n; i++)
        delete[] mt[i];
    delete[] mt;
}

// Из потока можно прочитать просто число, не надо для этого читать строку и парсить ее
// Исправлено:
void read_row_from_file(std::istream& _in, int* mt, int m)
{

    for (int j = 0; (j < m) && !_in.eof(); j++)
    {
        _in >> mt[j];
    }
}

int** read_from_file(int& n, int& m)
{
    std::string src = "src_file.txt";
    std::ifstream _in;
    _in.open(src);

    _in >> n;
    _in >> m;

    int** mt;

    mt = new int* [n];

    for (int i = 0; i < n; i++)
    {
        mt[i] = new int[m];

        read_row_from_file(_in, mt[i], m);
    }

    _in.close();
    return mt;
}

void read_row_from_console(int* mt, int m)
{
    for (int j = 0; j < m; j++)
        std::cin >> mt[j];
}

int** read_from_console(int& n, int& m)
{
    std::cout << "Введите количество строк матрицы"<< std::endl;
    std::cin >> n;
    std::cout << "Введите количество столбцов матрицы" << std::endl;
    std::cin >> m;

    int** mt;
    mt = new int* [n];

    for (int i = 0; i < n; i++)
    {
        mt[i] = new int[n];
        std::cout << "Введите элементы " << i << "-й строки матрицы" << std::endl;

        read_row_from_console(mt[i], m);
    }
    return mt;
}

int** read_data(int option, int& n, int& m)
{
    int** mt;
    switch (option)
    {
    case 1:
        return read_from_file(n, m);
        break;
    case 2:
        return read_from_console(n, m); 
        break;
    }
}

void write_result_in_file(std::string result)
{
    std::ofstream _out("result.txt");
    _out << result;
    _out.close();
    std::cout << "Файл был успешно создан";
}

void write_result_in_console(std::string result)
{
    std::cout << result;
}

void write_result(int option, std::string result)
{

    switch (option)
    {
    case 1:
        write_result_in_file(result);
        break;
    case 2:
        write_result_in_console(result);
    }
}


void menu()
{
    std::cout << "Введите" << std::endl << "1 - чтобы выполнить из файла" << std::endl << "2 - выполнить из консоли" << std::endl;
    int option = bounded_input(1, 2);

    int** mt = NULL;

    int n = 0, m = 0;

    mt = read_data(option, n, m);

    std::string result = "Максимальный среди элементов тех столбцов, которые упорядочены либо по возрастанию, либо по убыванию равен " + std::to_string(complete_task(mt, n, m));

    std::cout << "Выберите куда будет выполнен вывод результата" << std::endl << "1 - вывести в файл" << std::endl << "2 - вывести в консоль" << std::endl;
    option = bounded_input(1, 2);

    write_result(option, result);

    destroy(mt, n);
}